#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include "soubory.h"
#include "algoritmy.h"

using namespace std;

/**
 * @file prunik_mnozin.cpp
 * 
 * @brief Soubor, který obsahuje deklarace funkcí prunikMnozin, mereniCasu a funkci main
 */

/**
 * @brief Sestrojení průniku množin
 *
 * Funkce, která sestrojí průnik množin zadaný dvěma vektory
 *
 * @param mnozinaA První množina
 * @param mnozinaB Druhá množina
 * 
 * @return Vektor znázorňující průnik množin
 */
vector<int> prunikMnozin(vector<int>& mnozinaA, vector<int>& mnozinaB) {
    vector<int> prunik = {};
    bool pridana = false;

    for (int i = 0; i < mnozinaA.size(); i++) {    
        if (binarySearch(mnozinaB, mnozinaA[i]) != -1) {
            pridana = false;
            for (int j = 0; j < prunik.size(); j++) {
                if (mnozinaA[i] == prunik[j]) {
                    pridana = true;
                    break;
                }
            }
            if (!pridana)
                prunik.push_back(mnozinaA[i]);
        }
    }

    return prunik;
}

/**
 * @brief Změření času sestrojení průniku množin
 *
 * Funkce, která změří čas pro sestrojení průniku množin
 */
void mereniCasu() {
    auto zacatek = chrono::high_resolution_clock::now();
    vector<int> mnozinaA = nacteniSouboru("SourceSeqA.txt");
    vector<int> mnozinaB = nacteniSouboru("SourceSeqB.txt");

    shellSort(mnozinaA);
    shellSort(mnozinaB);

    vector<int> prunik = prunikMnozin(mnozinaA, mnozinaB);

    zapisDoSouboru(prunik);

    auto konec = chrono::high_resolution_clock::now();
    auto cas = chrono::duration_cast<chrono::seconds>(konec - zacatek);
    cout << "Cas trvani: " << cas.count() << " sekund." << endl;
}

int main() {
    mereniCasu();
    return 0;
}