#ifndef ALGORITMY_H
#define ALGORITMY_H
#pragma once

#include <vector>

using namespace std;

/**
 * @file algoritmy.h
 * 
 * @brief Soubor, který obsahuje prototypy funkcí pro práci s množinami
 */

/**
 * @brief Algoritmus Shell Sort
 *
 * Funkce, která slouží k seřazení vektoru pomocí algoritmu Shell Sort. Využil jsem pseudokód ze stránky https://www.tutorialspoint.com/data_structures_algorithms/shell_sort_algorithm.htm
 *
 * @param mnozina Neseřazený vektor
 */
void shellSort(vector<int>& mnozina);

/**
 * @brief Algoritmus Binary Search
 *
 * Funkce, která vyhledá hodnotu v seřazeném vektoru pomocí algoritmu Binary search. Využil jsem pseudokód z knížky Introduction to The Design and Analysis of Algorithms - Anany Levitin.
 *
 * @param mnozina Seřazený vektor
 * @param hodnota Hodnota, která se bude hledat v seřazeném vektoru
 * 
 * @return Pokud je hodnota nalezená, tak funkce vrátí index hodnoty ve vektoru, jinak vrátí -1
 */
int binarySearch(vector<int>& mnozina, int hodnota);

#endif