#include "algoritmy.h"

void shellSort(vector<int>& mnozina) {
    int interval = 0;
    while (interval < mnozina.size() / 3)
        interval = interval * 3 + 1;

    while (interval > 0) {
        for (int outer = interval; outer < mnozina.size(); outer++) {
            int value = mnozina[outer];
            int inner = outer;

            while (inner > interval - 1 && mnozina[inner - interval] >= value) {
                mnozina[inner] = mnozina[inner - interval];
                inner = inner - interval;
            }
            mnozina[inner] = value;
        }

        interval = (interval - 1) / 3;
    }
}

int binarySearch(vector<int>& mnozina, int hodnota) {
    int l = 0;
    int r = mnozina.size() - 1;

    while (l <= r) {
        int m = (l + r) / 2;
        if (hodnota == mnozina[m])
            return m;
        else if (hodnota < mnozina[m])
            r = m - 1;
        else
            l = m + 1;
    }

    return -1;
}