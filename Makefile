# Autor: Michal Kántor

CC=g++
CFLAGS= -g -Werror=vla

NAME = prunik_mnozin

all: $(NAME)

$(NAME): prunik_mnozin.cpp
	$(CC) $(CFLAGS) -o $(NAME) prunik_mnozin.cpp soubory.cpp algoritmy.cpp

clean:
	del $(NAME).exe