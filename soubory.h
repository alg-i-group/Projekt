#ifndef SOUBORY_H
#define SOUBORY_H
#pragma once

#include <vector>
#include <fstream>

using namespace std;

/**
 * @file soubory.h
 * 
 * @brief Soubor, který obsahuje prototypy funkcí pro práci se soubory
 */

/**
 * @brief Načtení množiny ze souboru
 *
 * Funkce, která načte hodnoty ze souboru a uloží je do vektoru
 *
 * @param cesta Cesta k souboru, ze kterého se má množina načíst
 * 
 * @return Vektor znázorňující množinu
 */
vector<int> nacteniSouboru(string cesta);

/**
 * @brief Zápis průniku množin do souboru
 *
 * Funkce, která sestrojený průnik množin zapíše do souboru
 *
 * @param prunik Vektor znázorňující průnik množin
 */
void zapisDoSouboru(vector<int>& prunik);

#endif