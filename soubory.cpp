#include <iostream>
#include "soubory.h"

vector<int> nacteniSouboru(string cesta) {
    fstream mnozina(cesta);

    if (!mnozina.is_open()) {
        cout << "Doslo k chybe pri otevirani souboru " + cesta;
        exit(1);
    }

    vector<int> vektor = {};
    int prvek;
    while (mnozina >> prvek) {
        vektor.push_back(prvek);
    }

    mnozina.close();
    return vektor;
}

void zapisDoSouboru(vector<int>& prunik) {
    fstream vysledek("prunik.txt", ios::out);
    for (int i = 0; i < prunik.size(); i++) {
        vysledek << prunik[i] << endl;
    }

    vysledek.close();
}